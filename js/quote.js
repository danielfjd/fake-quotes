export default class Quote {

    constructor() {
        this.currentQuestion = 1;
        this.answers = [];
        this.classes = {
            containerJS: 'js-container',
            questionWrapJS: 'js-question-wrap',
            questionJS: 'js-question',
            questionSectionJS: 'js-questions',
            questionCSS: 'question',
            buttonJS: 'js-true-false-button',
            resultsJS: 'js-results',
            scoreJS: 'js-score',
            maxJS: 'js-max',
            answersJS: 'js-answers',
            answersCSS: 'answers__answer',
            answersWrongCSS: 'answers__answer--wrong'

        };
        this.questions = this.retrieveQuestions();
        this.randomisedQuestions = this.questions
        .map((a) => ({sort: Math.random(), value: a}))
        .sort((a, b) => a.sort - b.sort)
        .map((a) => a.value);
        this.correctAnswers = 0;
    }

    init() {
     this.loadQuestions();
     this.initialiseEvents();
    }

    initialiseEvents() {
        var buttons = this.buttonElements;
        let questionElements = this.questionElements;
        buttons.forEach(button => {
            button.addEventListener('click', event => {
                this.answerQuestion(button);
            });
        });
    }

    loadQuestions() {

        let questionsWrap = this.questionWrapElement;

        // Randomise Questions

        this.randomisedQuestions.forEach((element, index) => {
            let randomQuestion = document.createElement('h2');
            randomQuestion.innerHTML = element.content;
            randomQuestion.classList.add(this.classes.questionCSS);
            randomQuestion.classList.add(this.classes.questionJS);
            randomQuestion.setAttribute('data-id', element.id);
            if(index === 0) {
                randomQuestion.style.display = 'block';
            }
            questionsWrap.appendChild(randomQuestion);
        });

    }

    answerQuestion(button) {

        let value = button.getAttribute('data-value');
        let questionElements = this.questionElements;
        let questionID = parseInt(questionElements[this.currentQuestion - 1].getAttribute('data-id'));
        let questionsCount = this.questions.length;
        let buttons = this.buttonElements;

        value = value === "true" ? true : value === "false" ? false : null;

        if(this.currentQuestion !== questionsCount) {

            if(!button.getAttribute('disabled')) {

                buttons[0].setAttribute('disabled', 'disabled');
                buttons[1].setAttribute('disabled', 'disabled');
    
                questionElements[this.currentQuestion - 1].style.display = 'none';
                questionElements[this.currentQuestion].style.display = 'block';
    
                this.answers.push({
                    id: questionID,
                    answer: value
                });

                this.currentQuestion++;
    
                buttons[0].removeAttribute('disabled');
                buttons[1].removeAttribute('disabled');
    
            }

        } else {
            this.answers.push({
                id: questionID,
                answer: value
            });
            this.questionSectionElement.style.display = 'none';
            this.showResults();
        }

    }

    showResults() {

        this.answers.forEach((answer, index) => {
            let answerElement = document.createElement('div');
            answerElement.classList.add(this.classes.answersCSS);
            if(answer.answer === this.randomisedQuestions[index].answer && this.randomisedQuestions[index].answer === true) {
                this.correctAnswers++;
                answerElement.innerHTML = `${this.randomisedQuestions[index].content} <span>It's real, you answered correctly</span>`;
            } else if(answer.answer === this.randomisedQuestions[index].answer && this.randomisedQuestions[index].answer === false) {
                this.correctAnswers++;
                answerElement.innerHTML = `${this.randomisedQuestions[index].content} <span>It's fake, you answered correctly</span>`;
            } else if(this.randomisedQuestions[index].answer === true) {
                answerElement.classList.add(this.classes.answersWrongCSS);
                answerElement.innerHTML = `${this.randomisedQuestions[index].content} <span>It's real, you answered incorrectly</span>`;
            } else if(this.randomisedQuestions[index].answer === false) {
                answerElement.classList.add(this.classes.answersWrongCSS);
                answerElement.innerHTML = `${this.randomisedQuestions[index].content} <span>It's fake, you answered incorrectly</span>`;
            }

            if(this.randomisedQuestions[index].image && this.randomisedQuestions[index].answer === true) {
                answerElement.innerHTML = `${answerElement.innerHTML} <img src="images/tweets/${answer.id}.png">`;
            }
            this.answersElement.appendChild(answerElement);
        });

        this.containerElement.classList.add('completed');
        this.resultsElement.style.display = 'block';
        this.scoreElement.innerHTML = ` ${this.correctAnswers} `;
        this.maxElement.innerHTML = ` ${this.questions.length}`;

    }

    retrieveQuestions() {
        let questions = [
            {
                id: 1, 
                content: '"Sorry losers and haters, but my I.Q. is one of the highest - and you all know it! Please don’t feel so stupid or insecure, it’s not your fault"', 
                answer: true, 
                image: 'images/tweets/1.png'
            },
            {
                id: 2, 
                content: '"Healthy young child goes to doctor, gets pumped with massive shot of many vaccines, doesn’t feel good and changes – AUTISM. Many such cases!"', 
                answer: true, 
                image: 'images/tweets/2.png'
            },
            {
                id: 3, 
                content: '"We should have gotten more of the oil in Syria, and we should have gotten more of the oil in Iraq. Dumb leaders."', 
                answer: true, 
                image: 'images/tweets/3.png'
            },
            {
                id: 4, 
                content: '"Every time I speak of the haters and losers I do so with great love and affection. They cannot help the fact that they were born fucked up!"', 
                answer: true, 
                image: 'images/tweets/4.png'
            },
            {
                id: 5, 
                content: '"Windmills are the greatest threat in the US to both bald and golden eagles. Media claims fictional "global warming" is worse."', 
                answer: true, 
                image: 'images/tweets/5.png'
            },
            {
                id: 6, 
                content: '"Shrek is overweight and Fioana is enabling him, sad."',
                answer: false
            },
            {
                id: 7, 
                content: '"Its freezing and snowing in New York, we need global warming!"',
                answer: false
            },
            {
                id: 8, 
                content: '"China should be like the great United States of America and build a wall to contain the Corona Virus!"',
                answer: false
            },
            {
                id: 9, 
                content: '"I am probably the greatest president of all time."',
                answer: false
            },
            {
                id: 10, 
                content: '"Hillary Clinton is a threat to this great nation. Makes me sick."',
                answer: false
            }
        ]
        return questions;
    }

    get containerElement() {
        return document.querySelector('.'+this.classes.containerJS);
    }

    get questionWrapElement() {
        return document.querySelector('.'+this.classes.questionWrapJS);
    }

    get questionSectionElement() {
        return document.querySelector('.'+this.classes.questionSectionJS);
    }
    
    get questionElements() {
        return document.querySelectorAll('.'+this.classes.questionJS);
    }

    get buttonElements() {
        return document.querySelectorAll('.'+this.classes.buttonJS);
    }
    
    get resultsElement() {
        return document.querySelector('.'+this.classes.resultsJS);
    }

    get scoreElement() {
        return document.querySelector('.'+this.classes.scoreJS);
    }

    get maxElement() {
        return document.querySelector('.'+this.classes.maxJS);
    }

    get answersElement() {
        return document.querySelector('.'+this.classes.answersJS);
    }
    
}