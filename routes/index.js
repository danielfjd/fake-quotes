var express = require('express');
var router = express.Router();

var homepage_controller = require('../controllers/homepageController');

/* GET home page. */
router.get('/', homepage_controller.home_page);

module.exports = router;